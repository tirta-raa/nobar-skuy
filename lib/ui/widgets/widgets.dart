import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:nobarskuy/shared/shared.dart';

part 'custom_input_text.dart';
part 'button/custom_button.dart';
part 'button/button_genre.dart';
part 'button/circle_botton.dart';
part 'rating_stars.dart';
part 'card/film_card.dart';
part 'card/coming_soon_card.dart';
part 'button/custom_botom_navbar.dart';
part 'card/artis_card.dart';
part 'selected_box.dart';
