part of 'widgets.dart';

class CustomInputText extends StatelessWidget {
  final String title;
  final String hintText;
  final bool obsecureText;

  CustomInputText({this.title, this.hintText, this.obsecureText = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 30, bottom: 6),
          child: Text(title, style: whiteTextStyle2.copyWith(fontSize: 14)),
        ),

        //* input text

        Container(
          width: 287,
          height: 55,
          margin: const EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(17)),
            boxShadow: [
              BoxShadow(
                color: Colors.white.withOpacity(0.7),
                spreadRadius: 1,
                blurRadius: 10,
                offset: Offset(0, 1), // changes position of shadow
              ),
            ],
          ),
          child: TextFormField(
            obscureText: obsecureText,
            decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              hintText: hintText,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(17),
                borderSide: BorderSide(
                  color: Color(0xffDBD7EC),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(17),
                borderSide: BorderSide(color: purpleColor, width: 2),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
