part of '../widgets.dart';

class CircleButton extends StatelessWidget {
  final Function onPressed;
  final Color color;

  CircleButton(
      {@required this.onPressed, this.color = const Color(0xFFFF5E2C)});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
          boxShadow: [
            BoxShadow(
              color: Colors.white.withOpacity(0.7),
              spreadRadius: 1,
              blurRadius: 70,
              offset: const Offset(0, 1), // changes position of shadow
            ),
          ],
          image: const DecorationImage(image: AssetImage('assets/arrow_r.png')),
        ),
      ),
    );
  }
}
