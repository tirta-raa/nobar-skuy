part of '../widgets.dart';

class ComingSoonCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 16),
      width: 100,
      height: 140,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        image: DecorationImage(
          image: NetworkImage(
            'https://cdn0-production-images-kly.akamaized.net/5qJqGCk1744Y3FShvYIdC7n5keI=/640x853/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3485854/original/033690000_1623975380-Approved_-_A4Poster_ENG.JPG',
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
