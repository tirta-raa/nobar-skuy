part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Sign Up',
      textStyle: whiteTextStyle1.copyWith(fontSize: 34),
      child: Column(
        children: [
          CustomInputText(
            title: 'Full Name',
            hintText: 'Enter your full name',
          ),
          CustomInputText(
            title: 'E-mail',
            hintText: 'Enter your e-mail',
          ),
          CustomInputText(
            obsecureText: true,
            title: 'Password',
            hintText: 'Enter your password',
          ),
          CustomButton(
            margin: EdgeInsets.symmetric(vertical: 47),
            width: 248,
            title: 'Sign Up',
            onPressed: () {
              Get.to(MainPage());
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Already have an account?',
                  style: whiteTextStyle2.copyWith(
                      fontSize: 14, color: Colors.white.withOpacity(0.9))),
              TextButton(
                onPressed: () {
                  Get.to(SignInPage());
                },
                child: Text('Sign In',
                    style: whiteTextStyle2.copyWith(
                        fontSize: 14, color: Colors.white.withOpacity(0.9))),
              )
            ],
          )
        ],
      ),
    );
  }
}
