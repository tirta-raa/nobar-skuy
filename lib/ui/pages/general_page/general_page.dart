part of '../pages.dart';

class GeneralPage extends StatelessWidget {
  final String title;
  final String subtitle;
  final TextStyle textStyle;
  final Function onBackButtonPressed;
  final DecorationImage profilePicture;
  final Widget child;
  final AssetImage backgroundImage;

  GeneralPage({
    this.title,
    this.subtitle,
    this.textStyle,
    this.onBackButtonPressed,
    this.profilePicture,
    this.child,
    this.backgroundImage = const AssetImage('assets/bg1.png'),
  });

  @override
  Widget build(BuildContext context) {
    Widget bgImage() {
      return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: backgroundImage,
            fit: BoxFit.cover,
          ),
        ),
      );
    }

    Widget body() {
      Widget lefftTitle() {
        return title != null
            ? Text(
                title,
                style: textStyle,
              )
            : const SizedBox();
      }

      Widget centerTitle() {
        return subtitle != null
            ? Text(
                subtitle,
                style: textStyle,
                textAlign: TextAlign.center,
              )
            : const SizedBox();
      }

      return SafeArea(
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(
                  defaultMargin, 30, defaultMargin, 0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: (subtitle != null)
                        ? MainAxisAlignment.center
                        : MainAxisAlignment.start,
                    children: [
                      lefftTitle(),
                      centerTitle(),
                    ],
                  ),
                  child ?? const SizedBox()
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget backButton() {
      return ListView(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: const EdgeInsets.only(left: defaultMargin, top: 30),
              child: onBackButtonPressed != null
                  ? GestureDetector(
                      onTap: () {
                        if (onBackButtonPressed != null) {
                          onBackButtonPressed();
                        }
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        margin: const EdgeInsets.only(right: 26),
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/arrow_l.png'),
                          ),
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
          ),
        ],
      );
    }

    Widget profileImage() {
      return profilePicture != null
          ? ListView(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: const EdgeInsets.only(left: defaultMargin, top: 30),
                    padding: const EdgeInsets.all(3),
                    width: 40,
                    height: 40,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff33FFB6),
                          Color(0xff3763FF),
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        image: profilePicture,
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
              ],
            )
          : SizedBox();
    }

    return Scaffold(
      body: Stack(
        children: [
          bgImage(),
          profileImage(),
          backButton(),
          body(),
        ],
      ),
    );
  }
}
