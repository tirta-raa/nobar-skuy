part of 'pages.dart';

class UserProfilingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Select Your\nFavorite Gendre',
      textStyle: whiteTextStyle1.copyWith(fontSize: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ButtonGenre(
                  title: 'Horror',
                ),
                ButtonGenre(
                  title: 'Horror',
                  status: 0,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ButtonGenre(
                  title: 'Horror',
                ),
                ButtonGenre(
                  title: 'Music',
                  status: 0,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ButtonGenre(
                  title: 'Action',
                  status: 0,
                ),
                ButtonGenre(
                  title: 'Drama',
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.symmetric(vertical: 40),
              child:
                  Text('Movie Language\nYou Prefer ?', style: whiteTextStyle1)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ButtonGenre(
                title: 'Bahsa',
                status: 0,
              ),
              ButtonGenre(
                title: 'English',
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ButtonGenre(
                  title: 'Japanses',
                  status: 0,
                ),
                ButtonGenre(
                  title: 'Korean',
                  status: 0,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(top: 40, bottom: 20),
              child: CircleButton(
                onPressed: () {},
              ),
            ),
          )
        ],
      ),
    );
  }
}
