part of 'pages.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Widget browseMovie() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 50,
            height: 50,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.7),
                  spreadRadius: 1,
                  blurRadius: 70,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              color: mainColor,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/action_ic.png'),
                ),
              ),
            ),
          ),
          Container(
            width: 50,
            height: 50,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.7),
                  spreadRadius: 1,
                  blurRadius: 70,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/tv_ic_normal.png'),
                ),
              ),
            ),
          ),
          Container(
            width: 50,
            height: 50,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.7),
                  spreadRadius: 1,
                  blurRadius: 70,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/heart_ic_normal.png'),
                ),
              ),
            ),
          ),
          Container(
            width: 50,
            height: 50,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.7),
                  spreadRadius: 1,
                  blurRadius: 70,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/kids_ic_normal.png'),
                ),
              ),
            ),
          ),
        ],
      );
    }

    return GeneralPage(
      backgroundImage: const AssetImage('assets/bg2.png'),
      profilePicture: DecorationImage(
        image: AssetImage('assets/default.png'),
        fit: BoxFit.cover,
      ),
      subtitle: 'Saldo \nIDR 300.000',
      textStyle: whiteTextStyle1.copyWith(fontSize: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 18),
              child: Text(
                'Helo, Tirta',
                style: whiteTextStyle1.copyWith(fontSize: 22),
              ),
            ),
          ),
          Text(
            'Let’s  Whatch New Movie',
            style: whiteTextStyle3.copyWith(fontSize: 16),
          ),
          Padding(
              padding: EdgeInsets.only(top: 34, bottom: 10),
              child: Text(
                'Browse Movie',
                style: whiteTextStyle2.copyWith(fontSize: 16),
              )),
          browseMovie(),
          Padding(
              padding: EdgeInsets.only(top: 28, bottom: 10),
              child: Text(
                'Now Playing',
                style: whiteTextStyle2.copyWith(fontSize: 16),
              )),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.to(DetailsPage());
                  },
                  child: FilmCard(),
                ),
              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 28, bottom: 10),
              child: Text(
                'Coming Soon',
                style: whiteTextStyle2.copyWith(fontSize: 16),
              )),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ComingSoonCard(),
                ComingSoonCard(),
                ComingSoonCard(),
                ComingSoonCard(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
