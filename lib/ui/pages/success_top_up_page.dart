part of 'pages.dart';

class SuccessTopUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 186),
            child: IlustrationPage(
              title: 'Emmm Yummy!',
              subtitle: 'You have successfully \ntop up the wallet',
              picturePath: 'assets/card.png',
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 62, bottom: 44),
            child: CustomButton(
              width: 250,
              title: 'My Wallet',
              onPressed: () {
                Get.to(MainPage());
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Discover new movie?',
                  style: whiteTextStyle2.copyWith(
                      fontSize: 14, color: Colors.white.withOpacity(0.9))),
              TextButton(
                onPressed: () {},
                child: Text('Back To Home',
                    style: whiteTextStyle2.copyWith(
                        fontSize: 14, color: mainColor.withOpacity(0.9))),
              )
            ],
          )
        ],
      ),
    );
  }
}
